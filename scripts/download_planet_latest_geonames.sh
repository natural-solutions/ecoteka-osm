#!/bin/bash

OSM_NAMES_LINK="https://github.com/OSMNames/OSMNames/releases/download/v2.0.4/planet-latest_geonames.tsv.gz"
OUTPUT="./data/planet-latest_geonames.tsv.gz"
DOCKER_NETWORK="ecoteka_osm"

function download {
  wget -O $1 $2
  gzip -d $1
}

function start {
  docker network create ${DOCKER_NETWORK}
  docker-compose up -d
}

function import_data {
  docker-compose exec db bash /tmp_data/import_data.sh
}

start
download $OUTPUT $OSM_NAMES_LINK
import_data