CREATE TABLE IF NOT EXISTS public.osmnames  (
    "name" text NULL,
    alternative_names text NULL,
    osm_type text NULL,
    osm_id text NULL,
    "class" text NULL,
    "type" text NULL,
    lon text NULL,
    lat text NULL,
    place_rank text NULL,
    importance text NULL,
    street text NULL,
    city text NULL,
    county text NULL,
    state text NULL,
    country text NULL,
    country_code text NULL,
    display_name text NULL,
    west text NULL,
    south text NULL,
    east text NULL,
    north text NULL,
    wikidata text NULL,
    wikipedia text NULL,
    housenumbers text NULL
);

TRUNCATE public.osmnames;

COPY public.osmnames FROM '/tmp_data/planet-latest_geonames.tsv' delimiter E'\t' CSV HEADER;